#A fun self-assignment that simulates the friendship paradox

#More details regarding this paradox and explanations of my choices
#should be in the explanations.pdf

import numpy as np
import matplotlib.pyplot as plt
import random

class Person(object):
    def __init__(self, attemptedFriendships):
        self.friendsList = list()
        self.attemptedFriendships = attemptedFriendships
        self.currentFriends = 0

    def receivedFriend(self, whichFriend):
        self.friendsList.append(whichFriend)
        self.currentFriends += 1


population = input("Please input the population (Suggesting 1000 - 10000): ")
#the number of friends that you ATTEMPTED to make
meanFriends = 5
sigma = 5

s = np.random.normal(meanFriends, sigma, population)
xAxis = set()

for whichPerson in xrange(population):
    if(s[whichPerson] < 0):
        s[whichPerson] = 0
    s[whichPerson] = round(s[whichPerson])
    xAxis.add(s[whichPerson])



count, bins, ignored = plt.hist(s, 30, normed=True)
plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) * np.exp( - (bins - meanFriends)**2 / (2 * sigma**2) ), linewidth=2, color='r')
plt.xlabel("Number of friendships initiated")
plt.ylabel("Population percentage")

xAxis = list(xAxis)
plt.xticks(xAxis)

print "  ! Close the plot window in order to continue !"
plt.show()

society = list()

print ">>> Creating society..."
for whichPerson in xrange(population):
   newPerson = Person(int(s[whichPerson]))
   society.append(newPerson)


print ">>> Making friendships..."
for whichPerson in xrange(population):
    # create a list of possible friends
    tempFriends = range(0, population)
    tempFriends = [x for x in tempFriends if x not in society[whichPerson].friendsList]     #remove any person that has befriended you
    tempFriends.remove(whichPerson)     #remove yourself

    #randomly choose friends
    for whichFriendship in xrange(society[whichPerson].attemptedFriendships):
        randFriend = random.choice(tempFriends)
        #update object attributes accordingly
        society[whichPerson].currentFriends += 1
        society[randFriend].currentFriends += 1

        society[whichPerson].friendsList.append(randFriend)
        society[randFriend].friendsList.append(whichPerson)
        #remove your new friend
        tempFriends.remove(randFriend)


#for whichPerson in xrange(population):
   # print "Person's: ", whichPerson, " friends are: ", society[whichPerson].friendsList

print ""
print">>> Society simulation ended. Calculating results..."
#k represents the number of people that the majority of their friends have more firends than them
k = 0

for whichPerson in xrange(population):
    moreThanYou = 0
    for whichFriend in society[whichPerson].friendsList:
        #increment if you have LESS friends than the whichFriend
        if society[whichFriend].currentFriends > society[whichPerson].currentFriends:
            moreThanYou += 1

        #decrement if you have MORE friends than the whichFriend
        elif society[whichFriend].currentFriends < society[whichPerson].currentFriends:
            moreThanYou -= 1

    if moreThanYou > 0:
        k += 1

percentage = float(k)/population * 100

print""
print ">>> Percentage of people that have less friends than most of their friends: ", percentage, "%"

totalFriends = 0
totalAvgsFriends = 0
popTemp = population
for whichPerson in society:
    totalFriends += whichPerson.currentFriends
    friendsFriends = 0
    for whichFriend in whichPerson.friendsList:
        friendsFriends += society[whichFriend].currentFriends
    if whichPerson.currentFriends != 0:
        totalAvgsFriends += float(friendsFriends) / whichPerson.currentFriends
    else:
        popTemp -= 1



print ">>> On average a person has ", float(totalFriends) / population, " friends while their friends on average have ", float(totalAvgsFriends) / popTemp, "friends"
