# Friendship Paradox Module

## A simple module that simulates the friendship paradox (explanation included)

*Your friends probably have more friends than you*


### Introduction
In this README you won't find much about the paradox. 
I suggest reading the explanations.pdf which includes useful links that explain the paradox and the way that I simulate it.


### Prerequisites
Python version: 'python2.7'  
Although I include a requirements.txt, which has all the packages used from my custom environment,   
you will only need `numpy` and `matplotlib`.  

### Running
1. Simply run the `main.py` module  
2. Input a preferable population (a population bigger than 10000 may have a slightly increased runtime)  
3. When the plot appears in order to continue the execution you must close the plot
